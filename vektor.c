/* Dooba SDK
 * Simple R/C tracked vehicle based on the Trax platform
 */

// External Includes
#include <string.h>
#include <net/ip.h>

// Internal Includes
#include "substrate.h"
#include "vektor.h"

// Socket
struct socket *vektor_sock;

// Main Init
void init()
{
	// Init Substrate
	substrate_init();

	// Clear Socket
	vektor_sock = 0;

	// Open UDP
	if(socket_open_udp(wl0, &vektor_sock, "0.0.0.0", VEKTOR_PORT, 0, vektor_sock_recv))								{ scli_printf("\n[!] Failed to open UDP socket on port %i\n", VEKTOR_PORT); vektor_fail(); }
}

// Main Loop
void loop()
{
	// Update Substrate
	substrate_loop();
}

// Socket Receive Handler
void vektor_sock_recv(void *user, struct socket *s, uint8_t *data, uint16_t size)
{
	int d[2];

	// Check Size
	if(size != VEKTOR_CHUNK_SIZE)																					{ scli_printf("\n[!] Received incomplete chunk: %i bytes\n", size); return; }

	// Get Data
	memcpy(d, data, size);

	// Set LED
	if((d[0] == 0) && (d[1] == 0))																					{ trax_led_b_off(); }
	else																											{ trax_led_b_on(); }

	// Update Tracks
	trax_set_k(d[0], d[1]);
}

// Fail
void vektor_fail()
{
	// Error
	for(;;)
	{
		trax_led_a_on();
		trax_led_b_off();
		_delay_ms(500);
		trax_led_a_off();
		trax_led_b_on();
		_delay_ms(500);
		substrate_loop();
	}
}
