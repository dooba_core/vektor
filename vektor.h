/* Dooba SDK
 * Simple R/C tracked vehicle based on the Trax platform
 */

#ifndef	__VEKTOR_H
#define	__VEKTOR_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <socket/socket.h>
#include <trax/trax.h>

// Control Socket Port
#define	VEKTOR_PORT							14455

// Data Chunk Size
#define	VEKTOR_CHUNK_SIZE					(sizeof(int) * 2)

// Socket
extern struct socket *vektor_sock;

// Main Init
extern void init();

// Main Loop
extern void loop();

// Socket Receive Handler
extern void vektor_sock_recv(void *user, struct socket *s, uint8_t *data, uint16_t size);

// Fail
extern void vektor_fail();

#endif
